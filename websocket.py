import asyncio
import logging
import json
import contextlib
import random
from aiohttp import web, websocket

loop = asyncio.get_event_loop()
log = logging.getLogger("game")
all_red_cards = ['0', '1', '2', '3', '4', '5', '6' ,'7' ,'8', '9', '10', '11', '12', '13', '14', '15', '16' ,'17', '18', '19', '20', '21']
# (0 - 8) attack (1 - 9)
# (9 - 17) defense (1 - 9)

red_cards = ['0', '1', '2', '3', '4', '5', '6' ,'7' ,'8', '9', '10', '11', '12', '13', '14', '15', '16' ,'17', '18', '19', '20', '21']
blue_cards = ['1', '2']



GAMES={}

def start_round(clients):
    log.info("Round started")
    for c in clients:
        c.send_str("start")


def give_card(red_cards, player_number, game_id):
    if 0 != len(red_cards):
        pass
    else:   
        red_cards[:] = all_red_cards
        random.shuffle(red_cards)
    sending_card = red_cards.pop()
    for hand_card in range(len(GAMES[game_id]['player_cards'][player_number])):
        if not GAMES[game_id]['player_cards'][player_number][hand_card]:
            GAMES[game_id]['player_cards'][player_number][hand_card] = sending_card
            return "take_" + str(hand_card) + "_" + sending_card
    GAMES[game_id]['player_cards'][player_number].append(sending_card)
    return "take_" + str(len(GAMES[game_id]['player_cards'][player_number]) - 1) + "_" + sending_card




def what_card(position, player_number, game_id):
    if (position == 300):
        return 0, GAMES[game_id]['player_cards'][player_number][0]
    elif (position == 370):
        return 1, GAMES[game_id]['player_cards'][player_number][1]
    elif (position == 440):
        return 2, GAMES[game_id]['player_cards'][player_number][2]
    elif (position == 510):
        return 3, GAMES[game_id]['player_cards'][player_number][3]
    elif (position == 580):
        return 4, GAMES[game_id]['player_cards'][player_number][4]
    elif (position == 650):
        return 5, GAMES[game_id]['player_cards'][player_number][5]
    elif (position == 720):
        return 6, GAMES[game_id]['player_cards'][player_number][6]
    elif (position == 790):
        return 7, GAMES[game_id]['player_cards'][player_number][7]
    elif (position == 860):
        return 8, GAMES[game_id]['player_cards'][player_number][8]
    elif (position == 930):
        return 9, GAMES[game_id]['player_cards'][player_number][9]
    return "what_card is bad"

def check_finish_game(game_id, player_number):
    if GAMES[game_id]['player_lives'][player_number] == 0:
        GAMES[game_id]["clients"][(player_number+1)%2].send_str("YOU WON THE GAME!")
        GAMES[game_id]["clients"][player_number].send_str("YOU LOST THE GAME.")
    elif GAMES[game_id]['player_lives'][(player_number+1)%2] == 0:
        GAMES[game_id]["clients"][(player_number+1)%2].send_str("YOU LOST THE GAME.")
        GAMES[game_id]["clients"][player_number].send_str("YOU WON THE GAME!")


def result_of_attack(player_number, card, game_id):
    if GAMES[game_id]['defense'][(player_number+1)%2] - 9 > card:
        return "los"
    elif GAMES[game_id]['defense'][(player_number+1)%2] - 9 < card:
        GAMES[game_id]['defense'][(player_number+1)%2] = 0
        GAMES[game_id]['player_lives'][(player_number+1)%2] -= 1
        check_finish_game(game_id, player_number)
        return "win"
    GAMES[game_id]['defense'][(player_number+1)%2] = 0
    return "dra"

def result_of_immediate_attack(card, player_number, game_id):
    attack = GAMES[game_id]['social'][(player_number+1)%2]
    GAMES[game_id]['social'][(player_number+1)%2] = 0
    if attack > card - 9:
        GAMES[game_id]['player_lives'][player_number] -= 1
        check_finish_game(game_id, player_number)
        return str(attack), 'win'
    else:
        return str(attack), 'los'

def defense(numer, card, player_number, game_id):
        GAMES[game_id]["defense"][player_number] = card
        GAMES[game_id]['player_cards'][player_number][numer] = None
        GAMES[game_id]["clients"][(player_number+1)%2].send_str("def"+ str(numer) + "_"+ str(card))
        log.info("defense" + str(player_number))
        return "you_use_def" +str(numer)

def attack(numer, card, player_number, game_id):
    result_of_att = result_of_attack(player_number, card, game_id)
    GAMES[game_id]['state'][player_number] = 2
    GAMES[game_id]['player_cards'][player_number][numer] = None
    GAMES[game_id]["clients"][(player_number+1)%2].send_str("att_"+ str(numer) + "_" + result_of_att + "_" + str(card))        
    log.info("attack" + str(player_number))
    return "you_use_att_" + result_of_att + "_" + str(numer)

def give_lives(game_id, player_number, numer):
    if GAMES[game_id]['player_lives'][player_number] > 3:
        return "use_another_card"
    else:
        GAMES[game_id]['player_lives'][player_number] += 1
        GAMES[game_id]['player_cards'][player_number][numer] = None
        GAMES[game_id]['clients'][(player_number+1)%2].send_str("rival_restore_" + str(numer))
        return "you_restore_" + str(numer) 

def social_engineering(game_id, player_number, numer):
    GAMES[game_id]["social"][player_number] = 2
    GAMES[game_id]["clients"][(player_number+1)%2].send_str("reset_"+str(numer))
    GAMES[game_id]["player_cards"][player_number][numer] = None
    return "you_choose_two_card_"+ str(numer) 

def sandbox(game_id, player_number, numer):
    GAMES[game_id]['player_cards'][player_number][numer] = None
    GAMES[game_id]['state'][(player_number+1)%2] = 5
    GAMES[game_id]["clients"][(player_number+1)%2].send_str("rival_use_sand_" + str(numer))
    return 'you_use_sand_' + str(numer)

def turn(position, player_number, game_id):
    GAMES[game_id]['social'][player_number] = 0
    numer, card = what_card(position, player_number, game_id)
    if card == None:
        return "I don't find this card. Something wrong."
    card = int(card)
    if GAMES[game_id]['state'][player_number] > 2:
        if GAMES[game_id]['state'][player_number] == 4:
            GAMES[game_id]["clients"][(player_number+1)%2].send_str("reset_" + str(numer))
            GAMES[game_id]['player_cards'][player_number][numer] = None
            return "leave_" + str(numer)
        elif GAMES[game_id]['state'][player_number] > 10:
            if (card < 9):
                GAMES[game_id]['whose_turn'] = (GAMES[game_id]['whose_turn'] + 1) % 2
                GAMES[game_id]["clients"][(player_number+1)%2].send_str("rival_imm_att_" + str(numer))
                GAMES[game_id]['state'][(player_number+1)%2] = 3
                GAMES[game_id]['player_cards'][player_number][numer] = None
                GAMES[game_id]['social'][player_number] = card
                return "you_imm_att_" + str(numer)
            else:
                return "Immediate access. Use Only attack"
        elif GAMES[game_id]['state'][player_number] == 5:
            if (card > 17 or card < 9):
                return "No! only defense!"
            else:
                return defense(numer, card, player_number, game_id)
        elif GAMES[game_id]['state'][player_number] == 3:
            if (card > 17 or card < 9):
                log.info("No only defense!" + str(player_number))
                return "No! only defense!"
            else:
                GAMES[game_id]['state'][player_number] = 1
                GAMES[game_id]['whose_turn'] = (GAMES[game_id]['whose_turn'] + 1) % 2
                if GAMES[game_id]['state'][(player_number+1)%2] < 10:
                    GAMES[game_id]["clients"][(player_number+1)%2].send_str("Your turn!")
                    GAMES[game_id]["clients"][player_number].send_str("Change_player")
                    return defense(numer, card, player_number, game_id)
                else:
                    GAMES[game_id]['state'][(player_number+1)%2] -= 10
                    GAMES[game_id]['player_cards'][player_number][numer] = None
                    imm_att, result = result_of_immediate_attack(card, player_number, game_id)
                    GAMES[game_id]["clients"][(player_number+1)%2].send_str("rival_imm_def_" + str(numer) + '_' + result +'_'+ str(card))#rival_imm_def_from_res_what
                    GAMES[game_id]["clients"][player_number].send_str('rival_imm_pic_' + imm_att)
                    return 'you_imm_def_' + str(numer) + '_' + result
    if (card < 9):
        if (GAMES[game_id]['state'][player_number] == 1):
            return attack(numer, card, player_number, game_id)
        else:
            return "You already use attack!"
    elif (card < 18):
        return defense(numer, card, player_number, game_id)
    elif (card == 18):
        return give_lives(game_id, player_number, numer)
    elif (card == 19):
        return social_engineering(game_id, player_number, numer)
    elif (card == 20):
        GAMES[game_id]["clients"][(player_number+1)%2].send_str("rival_access_"+ str(numer))
        GAMES[game_id]['state'][player_number] = GAMES[game_id]['state'][player_number] + 10
        GAMES[game_id]['player_cards'][player_number][numer] = None
        return "you_access_"+ str(numer)
    elif (card == 21):
        return sandbox(game_id, player_number, numer)
    return "turn is bad"

def check_amount_cards(game_id, player_number):
    amount_cards_on_hand = 0
    for card_in_hand in GAMES[game_id]['player_cards'][player_number]:
        if card_in_hand:
            amount_cards_on_hand+=1
    return amount_cards_on_hand < 5

def change_player(player_number, game_id):
    if GAMES[game_id]['state'][(player_number+1)%2] > 10:
        GAMES[game_id]['state'][(player_number+1)%2] -= 10
        imm_att, result = result_of_immediate_attack(0, player_number, game_id)
        GAMES[game_id]["clients"][(player_number+1)%2].send_str("rival_imm_def_n_" + result)#rival_imm_def_from_res_what
        GAMES[game_id]["clients"][player_number].send_str('rival_imm_pic_' + imm_att)
        GAMES[game_id]['state'][player_number] = 1
        GAMES[game_id]['whose_turn'] = (GAMES[game_id]['whose_turn'] + 1) % 2
        return 'you_imm_def_n_' + result
    elif GAMES[game_id]['state'][player_number] > 10:
        GAMES[game_id]['state'][player_number] -= 10
        GAMES[game_id]["clients"][(player_number+1)%2].send_str('rival_imm_fail')
        return 'you_imm_fail'
    elif GAMES[game_id]['state'][player_number] == 5:
        GAMES[game_id]['state'][player_number] = 1
        GAMES[game_id]["clients"][(player_number+1)%2].send_str("rival_free")
        GAMES[game_id]["clients"][player_number].send_str("you_free")
        GAMES[game_id]['whose_turn'] = (GAMES[game_id]['whose_turn'] + 1) % 2
        GAMES[game_id]["clients"][(player_number+1)%2].send_str("Your turn!")
        return "Change_player"
    else:
        GAMES[game_id]['social'][player_number] = 0
        if (check_amount_cards(game_id, player_number)):
            GAMES[game_id]['state'][player_number] = 1
            GAMES[game_id]['whose_turn'] = (GAMES[game_id]['whose_turn'] + 1) % 2
            if GAMES[game_id]['state'][(player_number+1)%2] == 5:
                GAMES[game_id]["clients"][(player_number+1)%2].send_str("you_in_sand")
                return 'rival_in_sand'
            GAMES[game_id]["clients"][(player_number+1)%2].send_str("Your turn!")
            return "Change_player"
        else:
            GAMES[game_id]["state"][player_number] = 4
            return "must_leave_from_card!"

def take_rival_card(position_card, player_number, game_id):
    if GAMES[game_id]["social"][player_number]:
        GAMES[game_id]["social"][player_number]-= 1
        if not GAMES[game_id]["social"][player_number]:
            GAMES[game_id]["clients"][player_number].send_str("you_finished_changed")
    else:
        return "you_finished_changed"
    numer, card = what_card(int(position_card), ((player_number + 1) % 2), game_id)
    GAMES[game_id]['player_cards'][(player_number + 1)%2][numer] = None
    for hand_card in range(len(GAMES[game_id]['player_cards'][player_number])):
        if not GAMES[game_id]['player_cards'][player_number][hand_card]:
            GAMES[game_id]["clients"][(player_number+1)%2].send_str("rival_pick_up_" + str(numer) + "_"+ str(hand_card))
            GAMES[game_id]['player_cards'][player_number][hand_card] = card
            return "you_get_" + str(numer) + "_" + str(hand_card) + "_" + card
    GAMES[game_id]['player_cards'][player_number].append(card)
    return "you_get_" + str(numer) +  "_" + str(len(GAMES[game_id]['player_cards'][player_number]) - 1) + "_" + card


def parse(text, red_cards, player_number, game_id):
    text = text.split('_')
    if (text[0] == "give"):
        if (text[1] == "red"):
            return give_card(red_cards, player_number, game_id)
    elif (text[0] == "want"):
        if player_number != GAMES[game_id]['whose_turn']:
            return "wait"
        if (text[1] == "go"):
            if text[2].isdigit():
                return turn(int(text[2]), player_number, game_id)
            else:
                return "bad position"
        elif (text[1] == "change"):
            if (text[2] == "player"):
                return change_player(player_number, game_id)
    elif (text[0] == 'take'):
        if text[1].isdigit():
            return take_rival_card(text[1], player_number, game_id)
        return 'bad position'
    elif (text[0] == "first"):
        return "first turn"
    else:
        return "I don't know this comand"

async def echo_ws_handler(request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)


    #Залогинивание
    game_id = request.match_info["game_id"]  # Номер игры(1)
    if game_id in GAMES:
        if (2 > len(GAMES[game_id]["player_lives"])):
            GAMES[game_id]["clients"].append(ws) # join game
            GAMES[game_id]["player_lives"].append(4) # join game
            start_round(GAMES[game_id]["clients"])
            player_number = 1
            state = True
            log.info("Second player joined")
            random.shuffle(red_cards)
    else:
        GAMES[game_id] = {"clients": [ws],
                          "player_lives": [4],
                          "player_cards": [[None, None, None, None, None, None, None, None], [None, None, None, None, None, None, None, None]],
                          "defense": [0, 0],
                          "whose_turn": 0,
                          "state": [3, 1],
                          "social": [0, 0]} # 3 - first turn 1 - attack, 2 - no attack, 4 - leave card, 6-sandbox/start new game
        player_number = 0
        state = True
        log.info("First player joined")
    #конец залогинивания

    async for msg in ws:
        if msg.tp == websocket.MSG_TEXT:
            log.info("Received msg %s", msg.data)
            sending_card = parse(msg.data, red_cards, player_number, game_id)
            ws.send_str(sending_card)
            log.info(GAMES[game_id]["defense"])
        elif msg.tp == websocket.MSG_CLOSE:
            log.info("Error: %s", str(msg))
            break
        else:
            log.info("Strange msg: %s", str(msg))

    log.info("Closing websocket connection")
    return ws



async def start_server():
    app = web.Application()
    app.router.add_route("GET", "/game/{game_id}", echo_ws_handler)
    server = await loop.create_server(app.make_handler(), '0.0.0.0', 8081)
    return server


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, format="%(asctime)s [%(levelname)s]: %(message)s")
    log.info("Starting server")

    loop.run_until_complete(start_server())
    loop.run_forever()

"""
async def start_server():
    app = web.Application()
    app.router.add_route("GET", "/echo", echo_ws_handler)
    server = await loop.create_server(app.make_handler(), '0.0.0.0', 8081)
    return server

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, format="%(asctime)s [%(levelname)s]: %(message)s")
    log.info("Starting server")

    loop.run_until_complete(start_server())
    loop.run_forever()
"""